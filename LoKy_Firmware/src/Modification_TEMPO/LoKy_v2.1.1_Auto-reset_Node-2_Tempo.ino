#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include "LowPower.h"
#include "2_setDataRate-sleep.h"
#include "3_Device_Keys.h"
#include <SoftwareSerial.h>
SoftwareSerial* LoKyTIC;

/* Linky option tarifaire  */
#define Linky_TEMPO true   
//#define Linky_BASE true 

/* Set time to reset LoKy  */
#define T_LoKy_reset 2 //in hour(s)

static osjob_t sendjob;
static float VccTIC;
char HHPHC;
int ISOUSC;               // intensité souscrite  
int IINST;                // intensité instantanée    en A
int PAPP;                 // puissance apparente      en VA

#ifdef Linky_TEMPO 
unsigned long BBRHCJB;       // compteur Heures Creuses  en W
unsigned long BBRHPJB;       // compteur Heures Pleines  en W
unsigned long BBRHCJW;       // compteur Heures Creuses  en W
unsigned long BBRHPJW;       // compteur Heures Pleines  en W
unsigned long BBRHCJR;       // compteur Heures Creuses  en W
unsigned long BBRHPJR;       // compteur Heures Pleines  en W
#endif

#ifdef Linky_BASE
unsigned long BASE;       // index BASE               en W
unsigned long BA_pre;       // Update 12/06
#endif

String PTEC;              // période tarif en cours
String DEMAIN;              // demain rouge
String ADCO;              // adresse du compteur
String OPTARIF;           // option tarifaire
int IMAX;                 // intensité maxi = 90A
String MOTDETAT;          // status word
String pgmVersion;        // TeleInfo program version
boolean teleInfoReceived;

char chksum(char *buff, uint8_t len);
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber);
char version[5] = "V2.01";

void(* resetFunc) (void) = 0; //declare reset function at address 0 - MUST BE ABOVE LOOP

// ---------------------------------------------- //
//      Read the Voltage regulated from TIC
// ---------------------------------------------- //
long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1126400L / result; // Back-calculate Vcc in mV
  return result;
}

// ---------------------------------------------- //
//   Update new values from TIC for the next TX 
// ---------------------------------------------- //
void updateParameters() {
  if (millis() >= (T_LoKy_reset*3600000)) resetFunc(); //Reset every T_LoKy_reset hours
  Serial.println("-----");
  Serial.println("Updating new LoKy_payload . . .");
  VccTIC = (int)(readVcc()); // returns VccTIC in mVolt for LoKy decoder
  Serial.print("VccTIC = "); Serial.print(VccTIC/1000); Serial.println(" V");
  LoKyTIC->begin(1200);  // Important!!! -> RESTART LoKyTIC 
  teleInfoReceived = readTeleInfo(true);
}


// ---------------------------------------------- //
//              LoRaWAN events (LMiC)
// ---------------------------------------------- //
void onEvent (ev_t ev) {
  Serial.print(os_getTime()); Serial.print(": ");
  
  switch (ev) {
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_BEACON_FOUND:
      Serial.println(F("EV_BEACON_FOUND"));
      break;
    case EV_BEACON_MISSED:
      Serial.println(F("EV_BEACON_MISSED"));
      break;
    case EV_BEACON_TRACKED:
      Serial.println(F("EV_BEACON_TRACKED"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;      
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      LMIC_setLinkCheckMode(0);
      break;
      
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      LMiC_Startup(); //Reset LMIC and retry
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      LMiC_Startup(); //Reset LMIC and retry
      break;
      
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE"));
      if (LMIC.txrxFlags & TXRX_ACK)
        Serial.println(F("Received ack"));
      if (LMIC.dataLen) {
        Serial.print(F("Received "));
        Serial.print(LMIC.dataLen);
        Serial.println(F(" bytes."));
      }
      delay(50);
      // Schedule next transmission
//      setDataRate();
      do_sleep(TX_INTERVAL);
      os_setCallback(&sendjob, do_send);
      
      

      break;
       
    case EV_LOST_TSYNC:
      Serial.println(F("EV_LOST_TSYNC"));
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_LINK_DEAD:
      Serial.println(F("EV_LINK_DEAD"));
      break;
    case EV_LINK_ALIVE:
      Serial.println(F("EV_LINK_ALIVE"));
      break;
      
    default:
      Serial.print(F("Unknown event: "));
      Serial.println((unsigned) ev);
      break;
  }
}

// ---------------------------------------------- //
//      Send the encrypted LoKy packet to TTN
// ---------------------------------------------- //
void do_send(osjob_t* j) {
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) {Serial.println(F("OP_TXRXPEND, not sending"));}
  else {
    // Update 12/06
    #ifdef Linky_TEMPO
    unsigned long BBRHCJB_pre = BBRHCJB;       // Update 12/06
    unsigned long BBRHPJB_pre = BBRHPJB;       // Update 12/06
    unsigned long BBRHCJW_pre = BBRHCJW;       // Update 12/06
    unsigned long BBRHPJW_pre = BBRHPJW;       // Update 12/06
    unsigned long BBRHCJR_pre = BBRHCJR;       // Update 12/06
    unsigned long BBRHPJR_pre = BBRHPJR;       // Update 12/06
    updateParameters();
    while (((BBRHCJB <= BBRHCJB_pre) && (BBRHPJB<=BBRHPJB_pre)&& (BBRHCJW<=BBRHCJW_pre)&& (BBRHPJW<=BBRHPJW_pre)&& (BBRHCJR<=BBRHCJR_pre)&& (BBRHPJR<=BBRHPJR_pre))|| ADCO == 000000000000){
      LoKyTIC->end();
      Serial.println(" * Re-read Linky...");
      updateParameters();
      }
    #endif

    // Update 12/06
    #ifdef Linky_BASE 
    BA_pre = BASE;
    updateParameters();
    while (BASE == 0 || ADCO == 000000000000){
      LoKyTIC->end();
      Serial.println(" * Re-read Linky...");
      updateParameters();
      }
    #endif
    
    if (teleInfoReceived) { displayTeleInfo();}
    int vc = VccTIC;    
    uint8_t is = IINST;
    uint16_t pa = PAPP;
    
    #ifdef Linky_TEMPO  
    uint32_t hcjb = BBRHCJB;
    uint32_t hpjb = BBRHPJB;
    uint32_t hcjw = BBRHCJW;
    uint32_t hpjw = BBRHPJW;
    uint32_t hcjr = BBRHCJR;
    uint32_t hpjr = BBRHPJR;
    unsigned char loky_data[46];
    #endif
  
    #ifdef Linky_BASE
    uint32_t be = BASE;
    unsigned char loky_data[16];
    #endif

    // LoKy payload arrangement for sending to TTN
    loky_data[0] = 0x0;         //data_type = 0 --> VccTIC   (2 bytes)
    loky_data[1] = vc >> 8;
    loky_data[2] = vc & 0xFF;
    loky_data[3] = 0x7;         //data_type = 7 --> IINST    (2 bytes)
    loky_data[4] = is >> 8;
    loky_data[5] = is & 0xFF;
    loky_data[6] = 0x9;         //data_type = 9 --> PAPP     (3 bytes)
    loky_data[7] = pa >> 16;
    loky_data[8] = pa >> 8;
    loky_data[9] = pa & 0xFF;
  
    #ifdef Linky_TEMPO  
    loky_data[10] = 0x01;       //data_type = 1 --> HCHPJB     (5 bytes)
    loky_data[11] = hcjb >> 32;
    loky_data[12] = hcjb >> 24;
    loky_data[13] = hcjb >> 16;
    loky_data[14] = hcjb >> 8;
    loky_data[15] = hcjb & 0xFF;
    loky_data[16]  = 0x2;       //data_type = 2 --> HCHCJB     (5 bytes)
    loky_data[17] = hpjb >> 32;
    loky_data[18] = hpjb >> 24;
    loky_data[19] = hpjb >> 16;
    loky_data[20] = hpjb >> 8;
    loky_data[21] = hpjb & 0xFF;  
    loky_data[22] = 0x03;       //data_type = 3 --> HCHPJW     (5 bytes)
    loky_data[23] = hcjw >> 32;
    loky_data[24] = hcjw >> 24;
    loky_data[25] = hcjw >> 16;
    loky_data[26] = hcjw >> 8;
    loky_data[27] = hcjw & 0xFF;
    loky_data[28]  = 0x4;       //data_type = 4--> HCHCJW     (5 bytes)
    loky_data[29] = hpjw >> 32;
    loky_data[30] = hpjw >> 24;
    loky_data[31] = hpjw >> 16;
    loky_data[32] = hpjw >> 8;
    loky_data[33] = hpjw & 0xFF; 
    loky_data[34] = 0x05;       //data_type = 5 --> HCHPJR     (5 bytes)
    loky_data[35] = hcjr >> 32;
    loky_data[36] = hcjr >> 24;
    loky_data[37] = hcjr >> 16;
    loky_data[38] = hcjr >> 8;
    loky_data[39] = hcjr & 0xFF;
    loky_data[40]  = 0x6;       //data_type = 6 --> HCHCJR     (5 bytes)
    loky_data[41] = hpjr >> 32;
    loky_data[42] = hpjr >> 24;
    loky_data[43] = hpjr >> 16;
    loky_data[44] = hpjr >> 8;
    loky_data[45] = hpjr & 0xFF; 
    #endif
   
    #ifdef Linky_BASE
    loky_data[10] = 0x08;       //data_type = 8 --> BASE     (5 bytes)
    loky_data[11] = be >> 32;
    loky_data[12] = be >> 24;
    loky_data[13] = be >> 16;
    loky_data[14] = be >> 8;
    loky_data[15] = be & 0xFF;    
    #endif
    
    LMIC_setTxData2(1, loky_data, sizeof(loky_data), 0);
    Serial.println(F("LoKy packet queued!!!"));
    
    LoKyTIC->end(); // Important!!! -> STOP LoKyTIC to send packet.
  }
  // Next TX is scheduled after TX_COMPLETE event.
}

// ---------------------------------------------- //
//      Called from setup to activate LoKy
// ---------------------------------------------- //
void LMiC_Startup() {
  os_init();
  // Reset the MAC state.
  LMIC_reset(); LMIC_setLinkCheckMode(1); LMIC_setAdrMode(1);  
  // Increase window time for clock accuracy problem
  LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);
  // Join the network, sending will be started after the event "Joined"
  LMIC_startJoining();
}

void setup() {
  Serial.begin(9600);
  TeleInfo(version);    // LoKyTIC init
  Serial.println("**--------------------------------**");
  Serial.println(F("        LoKy (re)starting"        ));
  delay(100);
//  Check_SupCapa();
  LMiC_Startup();       // LMiC init
  do_send(&sendjob);    // OTAA start
}

//** Fixed Main LOOP **//
void loop() {os_runloop_once();}
  
const int T_charge_SupCapa = 15; // Time to charge SupCapa
const long Vset_TIC = 3300; // The voltage set for the supercapacitor
// ---------------------------------------------- //
//    Sleep LoKy to charge the SuperCapacitor
// ---------------------------------------------- // 
void Check_SupCapa() {
  long value_readVCC = readVcc();
  Serial.print("Checking voltage read from TIC... : "); Serial.print(VccTIC/1000);Serial.println(" Volts");
  while (readVcc() < Vset_TIC) do_sleep(T_charge_SupCapa);
}
