Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

The Code is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

Shield: [![CC BY-NC-ND 4.0][cc-by-nc-nd-shield]][cc-by-nc-nd]

The Hardware is licensed under a
[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License][cc-by-nc-sa].


[![CC BY-NC-ND 4.0][cc-by-nc-nd-image]][cc-by-nc-nd]

[cc-by-nc-nd]: https://creativecommons.org/licenses/by-nc-nd/4.0/
[cc-by-nc-nd-image]: https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png
[cc-by-nc-nd-shield]: https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg
 

# LoKy

- LoKy is an Open-source project which creates a LoRaWAN end-device to connect with Linky meter in France. 
- The most recent firmware is V4
- Explanations are at https://miniprojets.net/index.php/2021/07/28/loky-open-source-projet-pour-linky/ 

![Alt text](https://miniprojets.net/wp-content/uploads/2021/06/image-11.png)


The Wifi version of this project can be found at https://miniprojets.net/index.php/2021/09/08/winky-open-source-projet-pour-linky-avec-wifi/

This End-device was created by Ferrari Jérôme and Ho Vu Duy Bao for CNRS/UGA/G-INP – G2ELAB

![CNRS-logo][]
![G2elab-logo][]
![UGA-logo][]
![GINP-logo][]


[GINP-logo]: https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble-INP-Etablissement-120px.png
[CNRS-logo]: https://upload.wikimedia.org/wikipedia/fr/thumb/7/72/Logo_Centre_national_de_la_recherche_scientifique_%282023-%29.svg/langfr-130px-Logo_Centre_national_de_la_recherche_scientifique_%282023-%29.svg.png
[G2elab-logo]: https://g2elab.grenoble-inp.fr/uas/alias31/LOGO/G2Elab+logo.png
[UGA-logo]:https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1D0TxrJin49KlU9Oo263OgjsanU9vKQZhKJ9LZsMjxxggnCEKO7NDsN-rW6CEXGJbLEE&usqp=CAU

